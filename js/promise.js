'use strict';

const getTodo2 = () => {
	return new Promise((resolve, reject) => {
		setTimeout(() => {
			let error = true;
			if (!error) {
				resolve({text: "Everything is OK!"})
			} else {
				reject();
			}
		}, 2000)
	})
};

getTodo2()
	.then((todo) => {
		console.log(todo.text)
	})
	.catch(() => {
		console.log("Something went wrong!")
	})
	.finally(() => {
		console.log("It's not my business if Promise is solved or rejected;)")
	});
