'use strict';

const getTodo3 = () => {
	return new Promise((resolve, reject) => {
		setTimeout(() => {
			let error = true;
			if (!error) {
				resolve({text: "Everything is OK!"})
			} else {
				reject();
			}
		}, 2000)
	})
};

async function fetchTodo() {
	try {
		const todo = await getTodo3();
		return todo
	} catch {
		console.log("Something went wrong!")
	}
	// const todo = await getTodo3();
	// return todo;
}

fetchTodo()
	.then(todo => console.log(todo.text))
	.catch(() => {console.log("Something went wrong!")})
	.finally(() => {console.log("It's not my business if Promise is solved or rejected;)")});
