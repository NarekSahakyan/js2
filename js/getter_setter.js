let person = {
	firstName: 'Jimmy',
	lastName: 'Smith',
	get fullName() {
		return this.firstName + ' ' + this.lastName;
	},
	set fullName (name) {
		let words = name.toString().split(' ');
		this.firstName = words[0] || '';
		this.lastName = words[1] || '';
	}
};

person.fullName = 'Jack Franklin';
console.log(person.firstName); // Jack
console.log(person.lastName); // Franklin
